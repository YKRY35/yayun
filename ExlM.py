import openpyxl

def read(xlsPath):
    wb = openpyxl.load_workbook(xlsPath)
    ws = wb['Sheet1']

    data = list(ws.values)

    print(data)
    return data


def main():
    read('data.xlsx')

if __name__ == '__main__':
    main()
