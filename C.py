from selenium.webdriver.common.keys import Keys
import time
import A
import ExlM
from Util import strCmp
import openpyxl
from Const import *
import os

driver = A.driver

EXCEL_FROM = "data.xlsx"
EXCEL_OUT = "data_output.xlsx"

TMP_DATA_FILE = "C_predata.txt"


def calcLink(line, gData):
    for g in gData:
        if strCmp(line[XIANGMU], g['xm']) and strCmp(line[CHANGCI], g['cc']) and \
                strCmp(line[PIAODANG], g['dc']) and A.zuoweiCmp(line[ZUOWEI], g['zw']):
            return g['link']
    return 'NotFound'


def getXms():
    mp = {}
    data = ExlM.read("data.xlsx")
    for line in data:
        mp[line[0]] = 1

    return list(mp.keys())


# def chooseLJFF():
#     A.chooseItem("通过链接分发", '//*[@id="ticketCert_list"]',
#                A.PREFIX + '/div[1]/div/form/div[4]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div')


def getLineDetails(line):
    curXM = A.extractData(line.find_element('xpath', './/td[3]/div/div[1]').text)
    curCC = A.extractData(line.find_element('xpath', './/td[3]/div/div[2]').text)
    curDC = A.extractData(line.find_element('xpath', './/td[2]/div/div[3]').text)
    curZuowei = A.extractData(line.find_element('xpath', './/td[2]/div/div[5]').text)

    return curXM, curCC, curDC, curZuowei


def loadGData(gData):
    with open(TMP_DATA_FILE, "r", encoding="utf-8") as f:
        lines = f.readlines()

        for line in lines:
            if line.strip() == "": continue
            items = line.split("_")
            # ww = 1
            gData.append({
                'xm': items[0], 'cc': items[1], 'dc': items[2], 'zw': items[3], 'link': items[4]
            })
        tt = 1


def saveTmpData(rcd):
    with open(TMP_DATA_FILE, "a", encoding="utf-8") as f:
        f.write(rcd)


def main():
    # allXms = getXms()

    # 获取全量数据
    gData = []
    tmpData = ""


    # for xm in allXms:
    #     A.chooseXM(xm)
    #     A.clickSearch()

    try:
        while True:
            lines = driver.find_elements('xpath',
                                         A.PREFIX + '/div[2]/div[3]/div/div/div/div/div/div/div/div/table/tbody/tr')
            for line in lines:
                try:
                    curXM, curCC, curDC, curZuowei = getLineDetails(line)
                except Exception:  # 如果失败了就等待1秒再次尝试
                    time.sleep(1.1)
                    # curXM, curCC, curDC, curZuowei = getLineDetails(line)
                    continue

                if A.check_element_exists('.//td[4]/div/div/button', line):
                    statusBtn = line.find_element('xpath', './/td[4]/div/div/button')
                    statusBtn.click()
                    time.sleep(1.7)

                    link = driver.find_element('xpath', "//*[contains(text(), '大客户身份识别码')]/../../div[2]/div").text

                    driver.find_element('xpath', '//*[@class="ant-modal-content"]/button[1]').click()
                    time.sleep(1.3)

                    rcd = "{}_{}_{}_{}_{}".format(curXM, curCC, curDC, curZuowei, link)
                    print(rcd)
                    gData.append({
                        'xm': curXM, 'cc': curCC, 'dc': curDC, 'zw': curZuowei, 'link': link
                    })
                    tmpData += rcd + "\n";

            if not A.clickNxtPageIfClickable():
                break
    except Exception as error:
        saveTmpData(tmpData)

        print("ERROR: ", error)
        exit(1)


    loadGData(gData)


    wb = openpyxl.load_workbook(EXCEL_FROM)
    ws = wb['Sheet1']
    data = list(ws.values)
    idx = 1
    for line in data:
        if line[ZHONGLEI] == '电子票':
            ws['Z{}'.format(idx)] = calcLink(line, gData)

        idx += 1
    wb.save(EXCEL_OUT)

    # 删除TMP文件
    os.remove(TMP_DATA_FILE)


if __name__ == '__main__':
    main()
