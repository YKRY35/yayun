
from selenium.webdriver.common.keys import Keys
import time
import ExlM
import Util

driver = Util.load()

PREFIX = '//*[@id="root"]/div/section/section/main/div/div/div/div/div[3]'

from Const import *
from Util import strCmp

# 检查元素是否存在
def check_element_exists(element, d = None):
    if d == None: d = driver
    try:
        d.find_element('xpath', element)
        return True
    except Exception as e:
        return False


def zuoweiCmp(exlData, pageData):
    exlData2 = exlData
    for k in (('楼:', ''), (' 区:', '|'), (' 排:', '|'), (' 座:', '|')):
        exlData2 = exlData2.replace(k[0], k[1])
    return strCmp(exlData2, pageData)

def tryClickItem(topInput, c):
    for i in range(0, 8):
        try:
            c.click()
            return True
        except Exception:
            pass
        topInput.send_keys(Keys.UP)

    for i in range(0, 8):
        topInput.send_keys(Keys.DOWN)

    for i in range(0, 8):
        try:
            c.click()
            return True
        except Exception:
            pass
        topInput.send_keys(Keys.DOWN)

def chooseItem(name, topInputXpath, itemXpath):
    topInput = driver.find_element('xpath', topInputXpath)
    topInput.click()

    for i in range(0, 7):
        for j in range(0, 8):
            topInput.send_keys(Keys.UP)
        time.sleep(0.1)

        cList = driver.find_elements('xpath', itemXpath)
        for c in cList:
            tn = c.get_attribute('title')
            # print(tn)
            if strCmp(tn, name):
                print("TEXT: " + c.text)
                # c.click()
                tryClickItem(topInput, c)
                return True

    print("ERROR: items not found : " + name)
    return False


def clickDelBtn(pt):
    delBtnXpath = pt
    if check_element_exists(delBtnXpath):
        driver.find_element('xpath', delBtnXpath).click()


def chooseXM(xm):
    clickDelBtn(PREFIX + '/div[1]/div/form/div[3]/div/div/div/div/span[2]/span')

    chooseItem(xm, '//*[@id="projectId"]',
               PREFIX + '/div[1]/div/form/div[3]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div')


def chooseCCMC(cc):
    chooseItem(cc, '//*[@id="eventId"]',
               PREFIX + '/div[1]/div/form/div[4]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div')


def choosePD(pd):
    chooseItem(pd, '//*[@id="priceId"]',
               PREFIX + '/div[1]/div/form/div[5]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div')


def chooseZH(zh):
    clickDelBtn(PREFIX + '/div[1]/div/form/div[6]/div/div/div/div/span[2]/span')

    chooseItem(zh, '//*[@id="kaAccountId"]',
               PREFIX + '/div[1]/div/form/div[6]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div')


# def findLine():

def clickSearch():
    driver.find_element('xpath', "//*[contains(text(), '搜 索')]").click()
    time.sleep(1.5)

def searchP(xm, cc, dangci, zhanghao):
    if xm != None:
        chooseXM(xm)
        if cc != None:
            chooseCCMC(cc)
            if dangci != None:
                choosePD(dangci)
    chooseZH(zhanghao)

    driver.find_element('xpath', PREFIX + '/div[1]/div/form/div[7]/div/div/div/button[1]').click()
    time.sleep(1.5)

def extractData(t: str):
    idx = t.index('：')
    return t[idx + 1:]

def buttonEnable(btn):
    return btn.get_attribute("disabled") == None

def nxtPageClickable():
    btn = driver.find_element('xpath', '//*[@title="下一页"]/button')
    return buttonEnable(btn)

def clickNxtPageIfClickable():
    btn = driver.find_element('xpath', '//*[@title="下一页"]/button')
    if btn.get_attribute("disabled") == None:  # 有下一页
        btn.click()
        time.sleep(1.5)
        return True
    else:
        return False

def findLineEle(xm, cc, dangci, zuowei):

    while True:
        lines = driver.find_elements('xpath', PREFIX + '/div[2]/div[2]/div/div/div/div/div/div/div/div/table/tbody/tr')

        inputEle = './/td[4]/div/div/div/span[1]/input'

        for line in lines:
            curXM = extractData(line.find_element('xpath', './/td[3]/div/div[1]').text)
            curCC = extractData(line.find_element('xpath', './/td[3]/div/div[2]').text)
            curDC = extractData(line.find_element('xpath', './/td[2]/div/div[3]').text)
            curZuowei = extractData(line.find_element('xpath', './/td[2]/div/div[5]').text)

            if  strCmp(curXM, xm) and strCmp(curCC, cc) and strCmp(curDC, dangci) and zuoweiCmp(zuowei, curZuowei) == True:
                return line

        if not clickNxtPageIfClickable():
            break

    return None

# 未设置 -- 确定电子
def weishezhi(xm, cc, pd, zw):
    # 筛选
    searchP(xm, cc, pd, '17481_工商银行_中国工商银行股份有限公司浙江省分行')

    # 找到那一行
    ele = findLineEle(xm, cc, pd, zw)

    if ele == None:
        print("ele=None: {}.{}.{}.{}".format(xm, cc, pd, zw))
        return

    # 调试
    print("{}.{}.{}.{}".format(xm, cc, pd, zw))
    print(ele.text)

    # 设置票
    ele.find_element('xpath', './/td[4]/div/div/div/span[1]/input').click()
    time.sleep(0.5)
    # 链接收集
    ele.find_element('xpath', './/td[4]/div/div/div[2]/div/div/div/div[2]/div/div/div[2]').click()
    time.sleep(0.5)

    # 确定

    conf = driver.find_element('xpath', "//*[contains(text(), '确 定')]")
    conf.click()
    time.sleep(3)



def main():

    data = ExlM.read('data.xlsx')

    for line in data:
        if line[ZHONGLEI] == '电子票':
            weishezhi(line[XIANGMU], line[CHANGCI], line[PIAODANG], line[ZUOWEI])



def test():
    # print(zuoweiCmp('楼:2F 区:N3 排:7排 座:15号', '2F|N3|7排|15号'))
    # print(zuoweiCmp('楼:2F 区:N3 排:7排 座:15号', '2F|N3|17排|15号'))

    searchP('游泳', '2023-09-27 星期三 10:00', 'A', '17481_工商银行_中国工商银行股份有限公司浙江省分行')
    ele = findLineEle('游泳', '2023-09-27 星期三 10:00', 'A', '楼:2F 区:N3 排:7排 座:16号')
    print(ele.text)

if __name__ == '__main__':
    main()
    # test()
