from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def strCmp(a:str, b:str):
    a = a.replace(" ", "")
    b = b.replace(" ", "")
    return a==b

def load():
    chrome_options = Options()
    # chrome_options.add_argument('headless')
    chrome_options.add_experimental_option("debuggerAddress", "127.0.0.1:9233")
    chrome_driver = "E:\\ChromeWebDriver\\chromedriver.exe"  # 指定自己的chromedriver路径
    driver = webdriver.Chrome(chrome_driver, chrome_options=chrome_options)
    print(driver.title)

    return driver