from selenium.webdriver.common.keys import Keys
import time
import A
import Util

driver = A.driver

def createLink(line):
    if not A.check_element_exists('.//td[4]/div/div/button', line):
        # print(line.text)
        return False

    btn = line.find_element('xpath', './/td[4]/div/div/button')

    if A.buttonEnable(btn):
        btn.click()
        time.sleep(0.5)

        kuangkuang = driver.find_element('xpath', "//*[contains(text(), '链接自创建起有效时间为72小时')]")

        confirm = kuangkuang.find_element('xpath', ".//../div[3]/button")
        confirm.click()
        time.sleep(1.5)

        driver.find_element('xpath', '//*[@class="ant-modal-content"]/button[1]').click()
        time.sleep(1.5)

        return True
    else:
        return False

def main():
    while True:
        lines = driver.find_elements('xpath', A.PREFIX+'/div[2]/div[3]/div/div/div/div/div/div/div/div/table/tbody/tr')

        found = False
        for line in lines:
            if createLink(line):
                found = True
                break

        if found:
            continue
        else: #尝试下一页
            if not A.clickNxtPageIfClickable(): # 没下一页
                break
            # 否则已经点了

        time.sleep(1.0)


if __name__ == '__main__':

    main()




